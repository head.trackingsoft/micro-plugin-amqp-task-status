<?php

namespace Micro\Plugin\AmqpTaskStatus\Plugin;

use Micro\Plugin\AmqpTaskStatus\Adapter\AmqpTaskStatusAdapterInterface;

interface AmqpTaskStatusAdapterProviderInterface
{
    /**
     * @return AmqpTaskStatusAdapterInterface
     */
    public function provideAmqpTaskStatusAdapter(): AmqpTaskStatusAdapterInterface;
}
