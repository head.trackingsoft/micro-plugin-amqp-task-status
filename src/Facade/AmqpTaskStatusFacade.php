<?php

namespace Micro\Plugin\AmqpTaskStatus\Facade;

use Micro\Plugin\AmqpTaskStatus\Model\TaskStatusInterface;

class AmqpTaskStatusFacade implements AmqpTaskStatusFacadeInterface
{
    /**
     * {@inheritDoc}
     */
    public function receiveTaskInformation(string $taskId): TaskStatusInterface
    {
    }
}
