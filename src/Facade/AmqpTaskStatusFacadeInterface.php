<?php

namespace Micro\Plugin\AmqpTaskStatus\Facade;

use Micro\Plugin\AmqpTaskStatus\Model\TaskStatusInterface;

interface AmqpTaskStatusFacadeInterface
{
    /**
     * @param string $taskId
     * @return TaskStatusInterface
     */
    public function receiveTaskInformation(string $taskId): TaskStatusInterface;
}
