<?php

namespace Micro\Plugin\AmqpTaskStatus\Exception;

use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class AdapterNotFoundException extends RuntimeException
{
    /**
     * @param string $adapterName
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $adapterName, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('AMQP Task Status Adapter "%s" is not registered.', $adapterName), $code, $previous);
    }
}
