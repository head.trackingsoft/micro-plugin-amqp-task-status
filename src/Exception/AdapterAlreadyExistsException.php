<?php

namespace Micro\Plugin\AmqpTaskStatus\Exception;

class AdapterAlreadyExistsException extends RuntimeException
{
    /**
     * @param string $adapterName
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $adapterName, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(sprintf('AMQP Task Status Adapter "%s" already exists.', $adapterName), $code, $previous);
    }
}
