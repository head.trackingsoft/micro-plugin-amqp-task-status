<?php

namespace Micro\Plugin\AmqpTaskStatus;

use Micro\Component\DependencyInjection\Container;
use Micro\Component\EventEmitter\ListenerProviderInterface;
use Micro\Framework\Kernel\Configuration\PluginConfigurationInterface;
use Micro\Framework\Kernel\Plugin\AbstractPlugin;
use Micro\Kernel\App\Business\ApplicationListenerProviderPluginInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterRepositoryFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterRepositoryFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterRepositoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\AppListener\ApplicationEventListenerFactory;
use Micro\Plugin\AmqpTaskStatus\Business\AppListener\ApplicationEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack\AckEventListenerFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack\AckEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack\NackEventListenerFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack\NackEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published\PublishedEventListenerFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published\PublishedEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received\ReceivedEventListenerFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received\ReceivedEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Provider\EventListenerProviderFactory;
use Micro\Plugin\AmqpTaskStatus\Facade\AmqpTaskStatusFacade;
use Micro\Plugin\AmqpTaskStatus\Facade\AmqpTaskStatusFacadeInterface;

/**
 * @method AmqpTaskStatusPluginConfigurationInterface configuration()
 */
class AmqpTaskStatusPlugin extends AbstractPlugin implements ApplicationListenerProviderPluginInterface
{
    /**
     * @var AdapterRepositoryInterface|null
     */
    private ?AdapterRepositoryInterface $adapterRepository = null;

    /**
     * {@inheritDoc}
     */
    public function provideDependencies(Container $container): void
    {
        $container->register(AmqpTaskStatusFacadeInterface::class, function (Container $container) {
            return $this->createAmqpTaskStatusFacade();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getEventListenerProvider(): ListenerProviderInterface
    {
        return $this->createEventListenerProviderFactory()->create();
    }

    /**
     * @return AmqpTaskStatusFacadeInterface
     */
    protected function createAmqpTaskStatusFacade(): AmqpTaskStatusFacadeInterface
    {
        return new AmqpTaskStatusFacade();
    }

    /**
     * @return EventListenerProviderFactory
     */
    protected function createEventListenerProviderFactory(): EventListenerProviderFactory
    {
        return new EventListenerProviderFactory(
            $this->createPublishedListenerFactory(),
            $this->createNackListenerFactory(),
            $this->createAckListenerFactory(),
            $this->createReceivedListenerFactory(),
            $this->createApplicationReadyEventListenerFactory()
        );
    }

    /**
     * @return AdapterRepositoryInterface
     */
    protected function getProviderRepository(): AdapterRepositoryInterface
    {
        if(!$this->adapterRepository) {
            $this->adapterRepository = $this->createAdapterRepositoryFactory()->create();
        }

        return $this->adapterRepository;
    }

    /**
     * @return AdapterResolverFactoryInterface
     */
    protected function createAdapterResolverFactory(): AdapterResolverFactoryInterface
    {
        return new AdapterResolverFactory(
            $this->getProviderRepository(),
            $this->configuration()
        );
    }

    /**
     * @return AdapterRepositoryFactoryInterface
     */
    protected function createAdapterRepositoryFactory(): AdapterRepositoryFactoryInterface
    {
        return new AdapterRepositoryFactory();
    }

    /**
     * @return ApplicationEventListenerFactoryInterface
     */
    protected function createApplicationReadyEventListenerFactory(): ApplicationEventListenerFactoryInterface
    {
        return new ApplicationEventListenerFactory(
            $this->getProviderRepository()
        );
    }

    /**
     * @return PublishedEventListenerFactoryInterface
     */
    protected function createPublishedListenerFactory(): PublishedEventListenerFactoryInterface
    {
        return new PublishedEventListenerFactory(
            $this->createAdapterResolverFactory()
        );
    }

    /**
     * @return NackEventListenerFactoryInterface
     */
    protected function createNackListenerFactory(): NackEventListenerFactoryInterface
    {
        return new NackEventListenerFactory(
            $this->createAdapterResolverFactory()
        );
    }

    /**
     * @return AckEventListenerFactoryInterface
     */
    protected function createAckListenerFactory(): AckEventListenerFactoryInterface
    {
        return new AckEventListenerFactory(
            $this->createAdapterResolverFactory()
        );
    }

    /**
     * @return ReceivedEventListenerFactoryInterface
     */
    protected function createReceivedListenerFactory(): ReceivedEventListenerFactoryInterface
    {
        return new ReceivedEventListenerFactory(
            $this->createAdapterResolverFactory()
        );
    }
}
