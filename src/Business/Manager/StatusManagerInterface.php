<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Manager;

use Micro\Plugin\Amqp\Business\Message\MessageInterface;

interface StatusManagerInterface
{
    /**
     * @param int $status
     * @param string $taskId
     *
     * @return void
     */
    public function changeStatus(string $taskId, int $status): void;

    /**
     * @param string $taskId
     * @param MessageInterface $message
     *
     * @return void
     */
    public function register(string $taskId, MessageInterface $message): void;
}
