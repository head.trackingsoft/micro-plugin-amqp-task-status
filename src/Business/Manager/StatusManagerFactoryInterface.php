<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Manager;

interface StatusManagerFactoryInterface
{
    /**
     * @return StatusManagerInterface
     */
    public function create(): StatusManagerInterface;
}
