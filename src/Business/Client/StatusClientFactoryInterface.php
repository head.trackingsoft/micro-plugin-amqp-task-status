<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Client;

interface StatusClientFactoryInterface
{
    /**
     * @return StatusClientInterface
     */
    public function create(): StatusClientInterface;
}
