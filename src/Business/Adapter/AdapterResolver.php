<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

use Micro\Plugin\AmqpTaskStatus\AmqpTaskStatusPluginConfigurationInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Client\StatusClientInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerInterface;

class AdapterResolver implements AdapterResolverInterface
{
    /**
     * @param AdapterRepositoryInterface $adapterRepository
     * @param AmqpTaskStatusPluginConfigurationInterface $amqpTaskStatusConfiguration
     */
    public function __construct(
    private AdapterRepositoryInterface $adapterRepository,
    private AmqpTaskStatusPluginConfigurationInterface $amqpTaskStatusConfiguration
    )
    {
    }

    /**
     * @return StatusClientInterface
     */
    public function resolveClient(): StatusClientInterface
    {
        $adapterName = $this->amqpTaskStatusConfiguration->getClientAdapterName();

        return $this->adapterRepository
            ->getAdapter($adapterName)
            ->getClientManagerFactory()
            ->create();
    }

    /**
     * @return StatusManagerInterface
     */
    public function resolveManager(): StatusManagerInterface
    {
        $adapterName = $this->amqpTaskStatusConfiguration->getManagerAdapterName();

        return $this->adapterRepository
            ->getAdapter($adapterName)
            ->getStatusManagerFactory()
            ->create();
    }
}
