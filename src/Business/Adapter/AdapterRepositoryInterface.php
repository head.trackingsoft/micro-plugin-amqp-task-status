<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

use Micro\Plugin\AmqpTaskStatus\Adapter\AmqpTaskStatusAdapterInterface;
use Micro\Plugin\AmqpTaskStatus\Exception\AdapterAlreadyExistsException;
use Micro\Plugin\AmqpTaskStatus\Exception\AdapterNotFoundException;

interface AdapterRepositoryInterface
{
    /**
     * @param string $adapterName
     *
     * @throws AdapterNotFoundException
     *
     * @return AmqpTaskStatusAdapterInterface
     */
    public function getAdapter(string $adapterName): AmqpTaskStatusAdapterInterface;

    /**
     * @param AmqpTaskStatusAdapterInterface $amqpTaskStatusAdapter
     *
     * @throws AdapterAlreadyExistsException
     *
     * @return self
     */
    public function appendAdapter(AmqpTaskStatusAdapterInterface $amqpTaskStatusAdapter): self;
}
