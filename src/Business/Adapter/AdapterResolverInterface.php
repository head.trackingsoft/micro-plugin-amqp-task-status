<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

use Micro\Plugin\AmqpTaskStatus\Business\Client\StatusClientInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerInterface;

interface AdapterResolverInterface
{
    /**
     * @return StatusClientInterface
     */
    public function resolveClient(): StatusClientInterface;

    /**
     * @return StatusManagerInterface
     */
    public function resolveManager(): StatusManagerInterface;
}
