<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

use Micro\Plugin\AmqpTaskStatus\Adapter\AmqpTaskStatusAdapterInterface;
use Micro\Plugin\AmqpTaskStatus\Exception\AdapterAlreadyExistsException;
use Micro\Plugin\AmqpTaskStatus\Exception\AdapterNotFoundException;

class AdapterRepository implements AdapterRepositoryInterface
{
    /**
     * @var array<string, AmqpTaskStatusAdapterInterface>
     */
    private array $adapterCollection;

    public function __construct()
    {
        $this->adapterCollection = [];
    }

    /**
     * {@inheritDoc}
     */
    public function getAdapter(string $adapterName): AmqpTaskStatusAdapterInterface
    {
        if(!array_key_exists($adapterName, $this->adapterCollection)) {
            throw new AdapterNotFoundException($adapterName);
        }

        return $this->adapterCollection[$adapterName];
    }

    /**
     * {@inheritDoc}
     */
    public function appendAdapter(AmqpTaskStatusAdapterInterface $amqpTaskStatusAdapter): AdapterRepositoryInterface
    {
        $adapterName = $amqpTaskStatusAdapter->getName();
        if(array_key_exists($adapterName, $this->adapterCollection)) {
            throw new AdapterAlreadyExistsException($adapterName);
        }

        $this->adapterCollection[$adapterName] = $amqpTaskStatusAdapter;

        return $this;
    }
}
