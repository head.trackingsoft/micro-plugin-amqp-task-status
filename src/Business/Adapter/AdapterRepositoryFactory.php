<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

class AdapterRepositoryFactory implements AdapterRepositoryFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function create(): AdapterRepositoryInterface
    {
        return new AdapterRepository();
    }
}
