<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

interface AdapterRepositoryFactoryInterface
{
    /**
     * @return AdapterRepositoryInterface
     */
    public function create(): AdapterRepositoryInterface;
}
