<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

use Micro\Plugin\AmqpTaskStatus\AmqpTaskStatusPluginConfigurationInterface;

class AdapterResolverFactory implements AdapterResolverFactoryInterface
{
    /**
     * @param AdapterRepositoryInterface $adapterRepository
     * @param AmqpTaskStatusPluginConfigurationInterface $amqpTaskStatusConfiguration
     */
    public function __construct(
    private AdapterRepositoryInterface $adapterRepository,
    private AmqpTaskStatusPluginConfigurationInterface $amqpTaskStatusConfiguration
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(): AdapterResolverInterface
    {
        return new AdapterResolver(
            $this->adapterRepository,
            $this->amqpTaskStatusConfiguration
        );
    }
}
