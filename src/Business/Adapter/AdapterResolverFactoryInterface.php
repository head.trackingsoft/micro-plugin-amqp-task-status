<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Adapter;

interface AdapterResolverFactoryInterface
{
    /**
     * @return AdapterResolverInterface
     */
    public function create(): AdapterResolverInterface;
}
