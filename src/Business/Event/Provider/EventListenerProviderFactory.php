<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Provider;

use Micro\Component\EventEmitter\ListenerProviderInterface;
use Micro\Plugin\AmqpTaskStatus\Business\AppListener\ApplicationEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack\AckEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack\NackEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published\PublishedEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received\ReceivedEventListenerFactoryInterface;
use Micro\Plugin\Storage\Business\Listener\Provider\EventListenerProviderFactoryInterface;
use Micro\Plugin\Storage\Business\Listener\Provider\EventListenerProviderInterface;

class EventListenerProviderFactory implements EventListenerProviderFactoryInterface
{
    /**
     * @param PublishedEventListenerFactoryInterface $publishedEventListenerFactory
     * @param NackEventListenerFactoryInterface $nackEventListenerFactory
     * @param AckEventListenerFactoryInterface $ackEventListenerFactory
     * @param ReceivedEventListenerFactoryInterface $receivedEventListenerFactory
     * @param ApplicationEventListenerFactoryInterface $applicationEventListenerFactory
     */
    public function __construct(
    private PublishedEventListenerFactoryInterface      $publishedEventListenerFactory,
    private NackEventListenerFactoryInterface           $nackEventListenerFactory,
    private AckEventListenerFactoryInterface            $ackEventListenerFactory,
    private ReceivedEventListenerFactoryInterface       $receivedEventListenerFactory,
    private ApplicationEventListenerFactoryInterface    $applicationEventListenerFactory
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(): EventListenerProviderInterface
    {
        return new EventListenerProvider(
            $this->publishedEventListenerFactory,
            $this->nackEventListenerFactory,
            $this->ackEventListenerFactory,
            $this->receivedEventListenerFactory,
            $this->applicationEventListenerFactory
        );
    }
}
