<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Provider;

use Micro\Component\EventEmitter\Impl\Provider\AbstractListenerProvider;
use Micro\Plugin\AmqpTaskStatus\Business\AppListener\ApplicationEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack\AckEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack\NackEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published\PublishedEventListenerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received\ReceivedEventListenerFactoryInterface;
use Micro\Plugin\Storage\Business\Listener\Provider\EventListenerProviderInterface;

final class EventListenerProvider extends AbstractListenerProvider implements EventListenerProviderInterface
{
    /**
     * @param PublishedEventListenerFactoryInterface $publishedEventListenerFactory
     * @param NackEventListenerFactoryInterface $nackEventListenerFactory
     * @param AckEventListenerFactoryInterface $ackEventListenerFactory
     * @param ReceivedEventListenerFactoryInterface $receivedEventListenerFactory
     */
    public function __construct(
    private PublishedEventListenerFactoryInterface      $publishedEventListenerFactory,
    private NackEventListenerFactoryInterface           $nackEventListenerFactory,
    private AckEventListenerFactoryInterface            $ackEventListenerFactory,
    private ReceivedEventListenerFactoryInterface       $receivedEventListenerFactory,
    private ApplicationEventListenerFactoryInterface    $applicationEventListenerFactory
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getEventListeners(): iterable
    {
        return [
            $this->publishedEventListenerFactory->create(),
            $this->nackEventListenerFactory->create(),
            $this->ackEventListenerFactory->create(),
            $this->receivedEventListenerFactory->create(),
            $this->applicationEventListenerFactory->create(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return 'amqp_task_status';
    }
}
