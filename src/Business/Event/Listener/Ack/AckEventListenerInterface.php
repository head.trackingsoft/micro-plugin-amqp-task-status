<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack;

use Micro\Component\EventEmitter\EventListenerInterface;

interface AckEventListenerInterface extends EventListenerInterface
{
}
