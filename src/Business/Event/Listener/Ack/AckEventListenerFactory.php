<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack;

use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;

class AckEventListenerFactory implements AckEventListenerFactoryInterface
{
    /**
     * @param AdapterResolverFactoryInterface $adapterResolverFactory
     */
    public function __construct(private AdapterResolverFactoryInterface $adapterResolverFactory)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(): AckEventListenerInterface
    {
        return new AckEventListener($this->adapterResolverFactory);
    }
}
