<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack;

interface AckEventListenerFactoryInterface
{
    /**
     * @return AckEventListenerInterface
     */
    public function create(): AckEventListenerInterface;
}
