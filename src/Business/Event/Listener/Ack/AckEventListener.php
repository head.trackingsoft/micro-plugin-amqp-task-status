<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Ack;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Plugin\Amqp\Event\AckMessageEventInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\AbstractReceivedListener;
use Micro\Plugin\AmqpTaskStatus\Model\TaskStatus;

class AckEventListener extends AbstractReceivedListener implements AckEventListenerInterface
{

    /**
     * {@inheritDoc}
     */
    protected function getMessageStatus(): int
    {
        return TaskStatus::APPLIED;
    }

    /**
     * {@inheritDoc}
     */
    public function supports(EventInterface $event): bool
    {
        return $event instanceof AckMessageEventInterface;
    }
}
