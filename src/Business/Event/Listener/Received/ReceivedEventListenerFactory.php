<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received;

use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Manager\StatusManagerFactoryInterface;

class ReceivedEventListenerFactory implements ReceivedEventListenerFactoryInterface
{
    /**
     * @param AdapterResolverFactoryInterface $statusManagerFactory
     */
    public function __construct(private AdapterResolverFactoryInterface $statusManagerFactory)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function create(): ReceivedEventListenerInterface
    {
        return new ReceivedEventListener($this->statusManagerFactory);
    }
}
