<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received;

use Micro\Component\EventEmitter\EventListenerInterface;

interface ReceivedEventListenerInterface extends EventListenerInterface
{
}
