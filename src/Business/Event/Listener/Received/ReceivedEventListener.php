<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Plugin\Amqp\Event\MessageReceivedEventInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\AbstractReceivedListener;
use Micro\Plugin\AmqpTaskStatus\Model\TaskStatus;

class ReceivedEventListener extends AbstractReceivedListener implements ReceivedEventListenerInterface
{
    /**
     * @param EventInterface $event
     *
     * @return bool
     */
    public function supports(EventInterface $event): bool
    {
        return $event instanceof MessageReceivedEventInterface;
    }

    /**
     * {@inheritDoc}
     */
    protected function getMessageStatus(): int
    {
        return TaskStatus::PROCESSING;
    }
}
