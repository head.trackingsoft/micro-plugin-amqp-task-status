<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Received;

interface ReceivedEventListenerFactoryInterface
{
    /**
     * @return ReceivedEventListenerInterface
     */
    public function create(): ReceivedEventListenerInterface;
}
