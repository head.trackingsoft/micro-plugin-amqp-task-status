<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack;

use Micro\Component\EventEmitter\EventListenerInterface;

interface NackEventListenerInterface extends EventListenerInterface
{
}
