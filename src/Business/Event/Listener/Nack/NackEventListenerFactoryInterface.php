<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack;

interface NackEventListenerFactoryInterface
{
    /**
     * @return NackEventListenerInterface
     */
    public function create(): NackEventListenerInterface;
}
