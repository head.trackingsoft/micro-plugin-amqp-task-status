<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Plugin\Amqp\Event\NackMessageEventInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\AbstractReceivedListener;
use Micro\Plugin\AmqpTaskStatus\Model\TaskStatus;

class NackEventListener extends AbstractReceivedListener implements NackEventListenerInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getMessageStatus(): int
    {
        return TaskStatus::REJECTED;
    }

    /**
     * {@inheritDoc}
     */
    public function supports(EventInterface $event): bool
    {
        return $event instanceof NackMessageEventInterface;
    }
}
