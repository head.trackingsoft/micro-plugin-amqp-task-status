<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Nack;

use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;

class NackEventListenerFactory implements NackEventListenerFactoryInterface
{
    /**
     * @param AdapterResolverFactoryInterface $adapterResolverFactory
     */
    public function __construct(private AdapterResolverFactoryInterface $adapterResolverFactory)
    {
    }
    /**
     * {@inheritDoc}
     */
    public function create(): NackEventListenerInterface
    {
        return new NackEventListener(
            $this->adapterResolverFactory
        );
    }
}
