<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published;

interface PublishedEventListenerFactoryInterface
{
    /**
     * @return PublishedEventListenerInterface
     */
    public function create(): PublishedEventListenerInterface;
}
