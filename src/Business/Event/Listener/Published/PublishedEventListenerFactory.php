<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published;

use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;

class PublishedEventListenerFactory implements PublishedEventListenerFactoryInterface
{
    /**
     * @param AdapterResolverFactoryInterface $adapterResolverFactory
     */
    public function __construct(
    private AdapterResolverFactoryInterface $adapterResolverFactory
    )
    {
    }

    /**
     * @return PublishedEventListenerInterface
     */
    public function create(): PublishedEventListenerInterface
    {
        return new PublishedEventListener(
            $this->adapterResolverFactory
        );
    }
}
