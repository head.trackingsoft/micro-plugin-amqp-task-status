<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Plugin\Amqp\Event\PublishMessageEvent;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Manager\StatusManagerFactoryInterface;

class PublishedEventListener implements PublishedEventListenerInterface
{
    /**
     * @param AdapterResolverFactoryInterface $adapterResolverFactory
     */
    public function __construct(private AdapterResolverFactoryInterface $adapterResolverFactory)
    {
    }

    /**
     * @param PublishMessageEvent $event
     *
     * @return void
     */
    public function on(EventInterface $event): void
    {
        $message = $event->getMessage();
        $id      = $message->getId();

        $this->adapterResolverFactory
            ->create()
            ->resolveManager()
            ->register($id, $message);
    }

    /**
     * {@inheritDoc}
     */
    public function supports(EventInterface $event): bool
    {
        return $event instanceof PublishMessageEvent;
    }
}
