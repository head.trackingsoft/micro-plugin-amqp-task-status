<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener\Published;

use Micro\Component\EventEmitter\EventListenerInterface;

interface PublishedEventListenerInterface extends EventListenerInterface
{

}
