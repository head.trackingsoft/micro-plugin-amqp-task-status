<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\Event\Listener;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Component\EventEmitter\EventListenerInterface;
use Micro\Plugin\Amqp\Event\AbstractMessageReceivedEvent;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterResolverFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerInterface;

abstract class AbstractReceivedListener implements EventListenerInterface
{
    /**
     * @param AdapterResolverFactoryInterface $adapterResolverFactory
     */
    public function __construct(protected AdapterResolverFactoryInterface $adapterResolverFactory)
    {
    }

    /**
     * @return int
     */
    abstract protected function getMessageStatus(): int;

    /**
     * @param AbstractMessageReceivedEvent $event
     *
     * @return void
     */
    public function on(EventInterface $event): void
    {
        $this->createStatusManager()->changeStatus(
            $event
                ->getMessage()
                ->content()
                ->getId(),
            $this->getMessageStatus()
        );
    }

    /**
     * @return StatusManagerInterface
     */
    protected function createStatusManager(): StatusManagerInterface
    {
        return $this->adapterResolverFactory
            ->create()
            ->resolveManager();
    }
}
