<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\AppListener;

use Micro\Component\EventEmitter\EventListenerInterface;

interface ApplicationEventListenerFactoryInterface
{
    /**
     * @return EventListenerInterface
     */
    public function create(): EventListenerInterface;
}
