<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\AppListener;

use Micro\Component\EventEmitter\EventInterface;
use Micro\Component\EventEmitter\EventListenerInterface;
use Micro\Kernel\App\Business\Event\ApplicationReadyEvent;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterRepositoryInterface;
use Micro\Plugin\AmqpTaskStatus\Plugin\AmqpTaskStatusAdapterProviderInterface;

class ApplicationReadyEventListener implements EventListenerInterface
{
    /**
     * @param AdapterRepositoryInterface $adapterRepository
     */
    public function __construct(private AdapterRepositoryInterface $adapterRepository)
    {
    }

    /**
     * @param ApplicationReadyEvent $event
     *
     * @return void
     */
    public function on(EventInterface $event): void
    {
        foreach ($event->kernel()->plugins() as $plugin) {
            if(!($plugin instanceof AmqpTaskStatusAdapterProviderInterface)) {
                continue;
            }

            $this->adapterRepository->appendAdapter($plugin->provideAmqpTaskStatusAdapter());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports(EventInterface $event): bool
    {
        return $event instanceof ApplicationReadyEvent;
    }
}
