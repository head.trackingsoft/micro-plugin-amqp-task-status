<?php

namespace Micro\Plugin\AmqpTaskStatus\Business\AppListener;

use Micro\Component\EventEmitter\EventListenerInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Adapter\AdapterRepositoryInterface;

class ApplicationEventListenerFactory implements ApplicationEventListenerFactoryInterface
{
    /**
     * @param AdapterRepositoryInterface $adapterRepository
     */
    public function __construct(private AdapterRepositoryInterface $adapterRepository)
    {
    }

    /**
     * @return EventListenerInterface
     */
    public function create(): EventListenerInterface
    {
        return new ApplicationReadyEventListener($this->adapterRepository);
    }
}
