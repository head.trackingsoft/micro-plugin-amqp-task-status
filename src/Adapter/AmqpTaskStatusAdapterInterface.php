<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter;

use Micro\Plugin\AmqpTaskStatus\Business\Client\StatusClientFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerFactoryInterface;

interface AmqpTaskStatusAdapterInterface
{
    /**
     * @return StatusManagerFactoryInterface
     */
    public function getStatusManagerFactory(): StatusManagerFactoryInterface;

    /**
     * @return StatusClientFactoryInterface
     */
    public function getClientManagerFactory(): StatusClientFactoryInterface;

    /**
     * @return string
     */
    public function getName(): string;
}
