<?php

namespace Micro\Plugin\AmqpTaskStatus;

interface AmqpTaskStatusPluginConfigurationInterface
{
    /**
     * @return string
     */
    public function getClientAdapterName(): string;

    /**
     * @return string
     */
    public function getManagerAdapterName(): string;
}
