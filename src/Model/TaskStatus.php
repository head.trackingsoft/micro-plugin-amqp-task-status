<?php

namespace Micro\Plugin\AmqpTaskStatus\Model;

interface TaskStatus
{
    public const PENDING    = 1;
    public const PROCESSING = 2;
    public const APPLIED    = 3;
    public const REJECTED   = 4;
}
