<?php

namespace Micro\Plugin\AmqpTaskStatus\Model;

use Micro\Plugin\Amqp\Business\Message\MessageInterface;
use Throwable;

interface TaskStatusInterface
{
    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface;

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface;

    /**
     * @return string
     */
    public function getTaskId(): string;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @return Throwable|null
     */
    public function getException(): ?Throwable;

    /**
     * @param Throwable|null $throwable
     *
     * @return self
     */
    public function setException(?Throwable $throwable): self;

    /**
     * @return MessageInterface
     */
    public function getMessage(): MessageInterface;
}
