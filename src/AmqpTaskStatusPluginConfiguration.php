<?php

namespace Micro\Plugin\AmqpTaskStatus;

use Micro\Framework\Kernel\Configuration\PluginConfiguration;

class AmqpTaskStatusPluginConfiguration extends PluginConfiguration implements AmqpTaskStatusPluginConfigurationInterface
{
    const CFG_ADAPTER_CLIENT  = 'AMQP_STATUS_ADAPTER_CLIENT';
    const CFG_ADAPTER_MANAGER = 'AMQP_STATUS_ADAPTER_MANAGER';
    const CFG_ADAPTER_COMMON  = 'AMQP_STATUS_ADAPTER';

    /**
     * {@inheritDoc}
     */
    public function getClientAdapterName(): string
    {
        return $this->configuration->get(self::CFG_ADAPTER_CLIENT, $this->getAdapterCommon());
    }

    /**
     * {@inheritDoc}
     */
    public function getManagerAdapterName(): string
    {
        return $this->configuration->get(self::CFG_ADAPTER_MANAGER, $this->getAdapterCommon());
    }

    /**
     * @return string
     */
    protected function getAdapterCommon(): string
    {
        return $this->configuration->get(self::CFG_ADAPTER_COMMON, null, false);
    }
}
